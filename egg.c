//this will be the egg() function.
//I (Logan) will do this part.

#include <stdio.h>

extern int save[500][500];

int MIN(int, int); //MIN function (returns smaller of two inputs)
int MAX(int, int); //MAX function (returns larger of two inputs)

int egg(int floors, int eggs){

	if(floors <= 0){
	
		return(0);
	
	}
	else if(floors == 1){
	
		return(1);
	
	}
	else if(eggs == 1){
	
		return(floors);
	
	}
	else{
	
		if(save[floors][eggs] == 0){ //if this scenario has not already been calculated, calculate it:
		
			int min = 9999999;
		
			for(int f=1; f<=floors; f++){ //for each floor
			
				int newEggs;
				
				if(eggs < 1){
				
					newEggs = 0;
				
				}
				else{
				
					newEggs = eggs-1;
				
				}
			
				if(save[f-1][newEggs] == 0){ //if the first half of the answer has not already been calculated
				
					save[f-1][newEggs] = egg(f-1, newEggs); //calculate it and save it
				
				}
			
				int result1 = save[f-1][newEggs]; //store the calculation into a variable for use in this function
			
				if(save[floors-f][eggs] == 0){ //if the second half has not been calculated
				
					save[floors-f][eggs] = egg(floors-f, eggs); //calculate and save it
				
				}
			
				int result2 = save[floors-f][eggs]; //save it into a variable
			
				int max = MAX(result1, result2); //save overall calculation into the array
	
				if(max < min){
				
					min = max;
				
				}
			
			}
		
			save[floors][eggs] = 1+min;
		
		}
	
		return(save[floors][eggs]); //return the answer
	
	}

}

int MIN(int a, int b){ //returns which of the two ints is smaller, a or b (returns -999 if they're equal)

	if(a<b) return(a); //a is smaller
	else return(b); //b is smaller, or they're equal. in this case, just return b.

}

int MAX(int a, int b){ //returns which of the two ints is bigger, a or b (returns -999 if they're equal)

	if(a>b) return(a); //a is greater
	else return(b); //b is greater, or they're equal. in this case, just return b.

}
